<?php
define('MENU_PASSTHRU_NONE', 0);
define('MENU_PASSTHRU_EXCLUDE', 1);
define('MENU_PASSTHRU_INCLUDE', 2);

/**
 * Implements hook_form_alter().
 */
function menu_passthru_form_alter(&$form, $form_state, $form_id) {
  switch ($form_id) {
    case 'menu_configure':
    case 'menu_edit_menu':
    case 'menu_edit_item':
      $menu_passthru = array(
        '#type' => 'fieldset',
        '#title' => t('Menu Pass thru'),
        '#tree' => TRUE,
        '#collapsible' => TRUE,
      );
      $menu_passthru_mode = array(
        '#type' => 'radios',
        '#options' => array(
          MENU_PASSTHRU_NONE => t('Use sitewide pass thru settings'),
          MENU_PASSTHRU_EXCLUDE => t('Pass thru all parameters except those listed'),
          MENU_PASSTHRU_INCLUDE => t('Pass thru only the parameters listed'),
        ),
        '#default_value' => MENU_PASSTHRU_INCLUDE,
      );
      $menu_passthru_list = array(
        '#type' => 'textarea',
        '#description' => t('Specify parameters that will be included or excluded from pass thru. One parameter per line.'),
      );

      switch ($form_id) {
        case 'menu_configure':
          $form['menu_passthru'] = $menu_passthru;
          $form['menu_passthru']['mode'] = $menu_passthru_mode;
          $form['menu_passthru']['list'] = $menu_passthru_list;
          unset($form['menu_passthru']['mode']['#options'][MENU_PASSTHRU_NONE]);
          $passthru_settings = menu_passthru_settings_load();
          $form['menu_passthru']['mode']['#default_value'] = $passthru_settings['mode'];
          $form['menu_passthru']['list']['#default_value'] = $passthru_settings['list'];
          break;

        case 'menu_edit_menu':
          $menu_name = str_replace('-', '_', $form['menu_name']['#default_value']);
          $form["menu_passthru_{$menu_name}"] = $menu_passthru;
          $form["menu_passthru_{$menu_name}"]['mode'] = $menu_passthru_mode;
          $form["menu_passthru_{$menu_name}"]['list'] = $menu_passthru_list;
          $passthru_settings = variable_get("menu_passthru_{$menu_name}", array(
            'mode' => MENU_PASSTHRU_NONE,
            'list' => '',
          ));
          $form["menu_passthru_{$menu_name}"]['mode']['#default_value'] = $passthru_settings['mode'];
          $form["menu_passthru_{$menu_name}"]['list']['#default_value'] = $passthru_settings['list'];
          $form['#submit'][] = 'menu_passthru_form_submit';
          break;

        case 'menu_edit_item':
          $item_mlid = $form['mlid']['#value'];
          $form["menu_passthru_{$item_mlid}"] = $menu_passthru;
          $form["menu_passthru_{$item_mlid}"]['mode'] = $menu_passthru_mode;
          $form["menu_passthru_{$item_mlid}"]['mode']['#options'][MENU_PASSTHRU_NONE] = t('Use item menu pass thru settings  ');
          $form["menu_passthru_{$item_mlid}"]['list'] = $menu_passthru_list;
          $passthru_settings = variable_get("menu_passthru_{$item_mlid}", array(
            'mode' => MENU_PASSTHRU_NONE,
            'list' => '',
          ));
          $form["menu_passthru_{$item_mlid}"]['mode']['#default_value'] = $passthru_settings['mode'];
          $form["menu_passthru_{$item_mlid}"]['list']['#default_value'] = $passthru_settings['list'];
          $form['#submit'][] = 'menu_passthru_form_submit';
          break;
      }
      break;
  }
}

function menu_passthru_form_submit($form, $form_state) {
  switch ($form['#form_id']) {
    case 'menu_edit_menu':
      $menu_name = str_replace('-', '_', $form_state['values']['menu_name']);
      variable_set("menu_passthru_{$menu_name}", $form_state['values']["menu_passthru_{$menu_name}"]);
      break;
    case 'menu_edit_item':
      $item_mlid = $form_state['values']['mlid'];
      variable_set("menu_passthru_{$item_mlid}", $form_state['values']["menu_passthru_{$item_mlid}"]);
      break;
  }
}

/**
 * Load sitewide passthru settings.
 */
function menu_passthru_settings_load() {
  $settings = variable_get('menu_passthru', array(
    'mode' => MENU_PASSTHRU_INCLUDE,
    'list' => '',
  ));

  return $settings;
}

/**
 * Load menu-based passthru settings.
 *
 * @param $menu
 *   The menu name.
 */
function menu_passthru_settings_menu_load($menu) {
  $settings = variable_get("menu_passthru_{$menu}", array(
    'mode' => MENU_PASSTHRU_NONE,
    'list' => '',
  ));

  if ($settings['mode'] == MENU_PASSTHRU_NONE) {
    $settings = menu_passthru_settings_load();
  }

  return $settings;
}

/**
 * Load item-based passthru settings.
 *
 * @param $item
 *   The menu item mlid.
 */
function menu_passthru_settings_item_load($mlid) {
  $settings = variable_get("menu_passthru_{$mlid}", array(
    'mode' => MENU_PASSTHRU_NONE,
    'list' => '',
  ));

  if ($settings['mode'] == MENU_PASSTHRU_NONE) {
    $menu_item = menu_link_load($mlid);
    $menu_name = str_replace('-', '_', $menu_item['menu_name']);
    $settings = menu_passthru_settings_menu_load($menu_name);
  }

  return $settings;
}

/**
 * Implements hook_permission().
 */
function menu_passthru_permission() {
  return array(
    'view menu passthru' => array(
      'title' => t('Menu passthru'),
      'description' => t('View menu links with passthru parameters.'),
    ),
  );
}

/**
 * Implements hook_block_view_alter().
 */
function menu_passthru_block_view_alter(&$data, $block) {
  switch ($block->module) {
    case 'menu':
      if (user_access('view menu passthru')) {
        menu_passthru_block_view_data($data);
      }
      break;
  }
}

/**
 * Add parameters to menu links.
 */
function menu_passthru_block_view_data(&$data) {
  $parameters = menu_passthru_current_parameters();

  foreach (element_children($data['content']) as $mlid) {
    $menu_link = &$data['content'][$mlid];
    $passthru_settings = menu_passthru_settings_item_load($mlid);
    $passthru_parameters = menu_passthru_parameters_filter($parameters, $passthru_settings);
    foreach ($passthru_parameters as $param => $value) {
      $menu_link['#localized_options']['query'][$param] = $value;
    }
  }
}

/**
 * Return parameters for current page.
 */
function menu_passthru_current_parameters() {
  $get = $_GET;
  unset($get['q']);
  return $get;
}

/**
 * Filter a list of parameters according with an array of passthru settings.
 *
 * @param $parameters
 *   Array of parameters in $_GET format.
 * @param $passthru_settings
 *   Array of passthru settings.
 *
 * @return
 *   A filtered out set of parameters.
 */
function menu_passthru_parameters_filter($parameters, $passthru_settings) {
  $mode = $passthru_settings['mode'];
  $list = preg_split('/\n|\r/', $passthru_settings['list'], -1, PREG_SPLIT_NO_EMPTY);

  switch ($mode) {
    case MENU_PASSTHRU_NONE:
      return array();

    case MENU_PASSTHRU_EXCLUDE:
      foreach ($parameters as $parameter => $value) {
        if (is_array($value)) {
          foreach ($value as $array_delta => $array_value) {
            if ((strpos($value, ':') !== FALSE)) {
              $subparameter = $parameter . ':' . current(explode(':', $array_value));
            }
            if (in_array($subparameter, $list, TRUE)) {
              unset($parameters[$parameter][$array_delta]);
            }
          }
        }
        if (in_array($parameter, $list, TRUE)) {
          unset($parameters[$parameter]);
        }
      }
      return $parameters;

    case MENU_PASSTHRU_INCLUDE:
      $return_parameters = array();
      foreach ($parameters as $parameter => $value) {
        if (is_array($value)) {
          foreach ($value as $array_delta => $array_value) {
            if ((strpos($value, ':') !== FALSE)) {
              $subparameter = $parameter . ':' . current(explode(':', $array_value));
            }
            if (in_array($subparameter, $list, TRUE)) {
              $return_parameters[$parameter][$array_delta] = $array_value;
            }
          }
        }
        if (in_array($parameter, $list, TRUE)) {
          $return_parameters[$parameter] = $value;
        }
      }
      return $return_parameters;
  }
}
